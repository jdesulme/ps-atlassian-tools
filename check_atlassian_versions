#!/usr/bin/env node

/*
  check_atlassian_versions

  Check version of different Atlassian products.
  Links deduced from links at https://community.atlassian.com/t5/Jira-questions/I-would-like-to-track-Atlassian-product-releases/qaq-p/308308

 */

const urls = [
    { name: 'JIRA Core', url: 'https://my.atlassian.com/download/feeds/current/jira-core.json' },
    { name: 'JIRA Software', url: 'https://my.atlassian.com/download/feeds/current/jira-software.json' },
    { name: 'JIRA Service Desk', url: 'https://my.atlassian.com/download/feeds/current/jira-servicedesk.json' },
    { name: 'Confluence', url: 'https://my.atlassian.com/download/feeds/current/confluence.json' },
    { name: 'Bitbucket Server', url: 'https://my.atlassian.com/download/feeds/current/stash.json'},
    { name: 'Bamboo', url: 'https://my.atlassian.com/download/feeds/current/bamboo.json'},
    { name: 'FishEye', url: 'https://my.atlassian.com/download/feeds/current/fisheye.json'},
    { name: 'Crucible', url: 'https://my.atlassian.com/download/feeds/current/crucible.json'}
];

const request = require('request');
const vm = require('vm');
const async = require('async');
const result = require('sorted-array').comparing(function(a){return a.product;},[]);

function getVersion(entry,callback) {
    request(entry.url, function(error, response, body) {
        if(!error && response.statusCode === 200) {
            jsonpSandbox = vm.createContext({ downloads: function (r) { return r; }});
            const r = vm.runInContext(body, jsonpSandbox);
            result.insert({product:entry.name,version:r[0].version});
            callback(null);
        } else {
            callback(error);
        }
    });
}

function printResult(error) {
    if (error) {
        console.error(error);
        return;
    }
    for (var i=0; i<result.array.length; i++) {
        var e=result.array[i];
        var indent = '\t';
        for (var j=2-(e.product.length/8); j>0; j--)
            indent += '\t';
        console.log(e.product + indent + e.version);
    }
}

async.each(urls,getVersion,printResult);